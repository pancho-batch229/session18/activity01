// console.log("HELLO!");

/*  -----ADD FUNCTION-----  */
// use of parameter and arguments
function addition(num1, num2) {
	let sum = num1 + num2;
	console.log("The sum of " + num1 + " and " + num2 + " is...");
	console.log(sum);
}

addition(10, 25);
addition(32, 324);

/*  -----SUBTRACT FUNCTION-----  */
// use of parameter and arguments
function subtraction(num1, num2) {
	let difference = num1 - num2;
	console.log("The difference of " + num1 + " and " + num2 + " is...");
	console.log(difference);
}

subtraction(10, 9);
subtraction(1230, 3201);

/*  -----MULTIPLY FUNCTION-----  */
// use of return statement 
function multiplication(num1, num2) {
	let product = num1 * num2;
	console.log("The product of " + num1 + " and " + num2 + " is...");
	return product;
}

let returnedProduct = multiplication(10, 10);
console.log(returnedProduct);

/*  -----DIVIDE FUNCTION-----  */
// use of return statement 
function division(num1, num2) {
	let quotient = num1 / num2;
	console.log("The quotient of " + num1 + " and " + num2 + " is...");
	return quotient;
}

let returnedQuotient = division(10, 10);
console.log(returnedQuotient);

/*  -----AREA FUNCTION-----  */
function getCircleRadius(givenCircleRadius) {
	let circleArea = 3.14 * (givenCircleRadius ** 2);
	console.log("The area of the given radius (" + givenCircleRadius + ") is...");
	return circleArea;
}

let CircleRadius = getCircleRadius(3);
console.log(CircleRadius);

/*  -----AVERAGE FUNCTION-----  */
function getAverage(w, x, y, z) {
	let average = (w + x + y + z) / 4;
	console.log("The average of " + w + ", " + x + ", " + y + ", and " + z + " is... ");
	return average;
}

let averageVar = getAverage(32, 56, 20, 85);
console.log(averageVar);

/*  -----GRADING FUNCTION-----  */
function checkScore(score, total) {
	console.log("Is " + score + "/" + total + " a passing score?");
	return (score/total)*100 > 75;
}

let isPassed = checkScore(85,100);
console.log(isPassed);
